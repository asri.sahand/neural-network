

function res = my_func(x, y, z) 
    soorat = x - (y^3)*z + 3*(z^2)*y;
    makhraj = (x-y)^2 + (y-z)^3 + (z-x)^4;
    res = soorat/makhraj;
end