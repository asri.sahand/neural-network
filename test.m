
input_num = 50;
input_test = rand(3, input_num);
output_test = zeros(1,input_num);

for i = 1:input_num
   tmp = input_test(:, i);
   res = my_func(tmp(1), tmp(2), tmp(3));
   output_test(i) = res;
end