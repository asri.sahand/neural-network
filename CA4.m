
% input is a 3*input_num matrix
% and each column stores x, y, z
% output is a input_num matrix

input_num = 300;

% static variable increase speed
result = zeros(1,input_num);
result_neural = result;
% create input matrix with random numbers
input = rand(3, input_num);
input_copy = input;
for i = 1:input_num
   tmp = input(:, i);
   res = my_func(tmp(1), tmp(2), tmp(3));
   result(i) = res;
end